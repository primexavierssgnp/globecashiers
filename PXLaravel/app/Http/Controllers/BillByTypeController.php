<?php

namespace App\Http\Controllers;

use App\Model\BillByType;
use Illuminate\Http\Request;

class BillByTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\BillByType  $billByType
     * @return \Illuminate\Http\Response
     */
    public function show(BillByType $billByType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\BillByType  $billByType
     * @return \Illuminate\Http\Response
     */
    public function edit(BillByType $billByType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\BillByType  $billByType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BillByType $billByType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\BillByType  $billByType
     * @return \Illuminate\Http\Response
     */
    public function destroy(BillByType $billByType)
    {
        //
    }
}
