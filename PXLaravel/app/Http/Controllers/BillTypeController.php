<?php

namespace App\Http\Controllers;

use App\Model\BillType;
use Illuminate\Http\Request;
use App\DataTables\BillTypeDataTable;

class BillTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BillTypeDataTable $dataTable)
    {
        return $dataTable->render('backend.billType.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.billType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\BillType  $billType
     * @return \Illuminate\Http\Response
     */
    public function show(BillType $billType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\BillType  $billType
     * @return \Illuminate\Http\Response
     */
    public function edit(BillType $billType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\BillType  $billType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BillType $billType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\BillType  $billType
     * @return \Illuminate\Http\Response
     */
    public function destroy(BillType $billType)
    {
        //
    }
}
