<?php

namespace App\Http\Controllers;

use App\DataTables\LevelDataTable;
use App\Model\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LevelDataTable $dataTable)
    {
        return $dataTable->render('backend.level.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function show(Level $level,$id)
    {
        $data = $level->where('id',$id)->first();

        return view('backend.level.view')->with("data",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function edit(Level $level)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Level $level)
    {
        $data = $level->where("id",$request->id)->first();
        if($data){
            return view('backend.level.update')->with("data",$data);
        }else{
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        //
    }
}
