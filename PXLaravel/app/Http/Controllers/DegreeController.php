<?php

namespace App\Http\Controllers;

use App\DataTables\DegreeDataTable;
use Illuminate\Http\Request;
use App\Model\Degree;

class DegreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DegreeDataTable $dataTable)
    {
        return $dataTable->render('backend.degree.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.degree.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newdata = new Degree;
        $newdata->name = $request->name;
        $newdata->desc = $request->desc;
        $newdata->is_active = $request->has('is_active');
        if($newdata->save()){
            return redirect()->route('backend-degree-show', ['id' => $newdata->id]);
        }else{
            return Redirect::back()->withInput()->withErrors(['msg', 'The Message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function show(Degree $degree, $id)
    {
        $data = $degree->where("id",$id)->first();
        if($data){
            return view('backend.degree.view')->with("data",$data);
        }else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function edit(Degree $degree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Degree $degree)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Degree $degree)
    {
        //
    }
}
