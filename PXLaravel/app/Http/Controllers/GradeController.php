<?php

namespace App\Http\Controllers;

use App\DataTables\GradeDataTable;
use Illuminate\Http\Request;
use App\Model\Grade;
use App\Model\Degree;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GradeDataTable $dataTable)
    {
        return $dataTable->render('backend.grade.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Degree::get()->count() > 0){
            $degrees = Degree::get();
            return view('backend.grade.create')->with("degrees",$degrees);
        }else{
            return redirect()->route('backend-grade');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newGrade = new Grade;
        $newGrade->name = $request->name;
        $newGrade->desc = $request->desc;
        $newGrade->degree_id = $request->degree_id;
        $newGrade->level = $request->level;
        $newGrade->is_active = $request->has('is_active');
        if($newGrade->save()){            
            return redirect()->route('backend-grade-show', ['id' => $newGrade->id]);
        }else{
            return Redirect::back()->withInput()->withErrors(['msg', 'The Message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade, $id)
    {
        $data = $grade->where("id",$id)->first();
        if($data){
            return view('backend.degree.view')->with("data",$data);
        }else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Grade $grade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grade $grade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        //
    }
}
