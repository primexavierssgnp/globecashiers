<?php

namespace App\Http\Controllers;

use App\Model\BillStudent;
use Illuminate\Http\Request;

class BillStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\BillStudent  $billStudent
     * @return \Illuminate\Http\Response
     */
    public function show(BillStudent $billStudent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\BillStudent  $billStudent
     * @return \Illuminate\Http\Response
     */
    public function edit(BillStudent $billStudent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\BillStudent  $billStudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BillStudent $billStudent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\BillStudent  $billStudent
     * @return \Illuminate\Http\Response
     */
    public function destroy(BillStudent $billStudent)
    {
        //
    }
}
