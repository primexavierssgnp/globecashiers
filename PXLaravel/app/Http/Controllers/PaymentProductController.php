<?php

namespace App\Http\Controllers;

use App\Model\PaymentProduct;
use Illuminate\Http\Request;

class PaymentProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\PaymentProduct  $paymentProduct
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentProduct $paymentProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\PaymentProduct  $paymentProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentProduct $paymentProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\PaymentProduct  $paymentProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentProduct $paymentProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\PaymentProduct  $paymentProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentProduct $paymentProduct)
    {
        //
    }
}
