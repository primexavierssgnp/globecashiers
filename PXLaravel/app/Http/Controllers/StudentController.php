<?php

namespace App\Http\Controllers;

use App\DataTables\StudentDataTable;
use App\Model\Student;
use App\Model\Grade;
use Illuminate\Http\Request;
use Carbon\Carbon;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StudentDataTable $dataTable)
    {
        return $dataTable->render('backend.student.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Grade::get()->count() > 0){
            $grades = Grade::get();
            return view('backend.student.create')->with("grades",$grades);            
        }else{
            return redirect()->route('backend-student');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $newStudent = new Student;
        $newStudent->NIS = $request->nis;
        $newStudent->NISN = $request->nisn;
        $newStudent->name = $request->name;
        $newStudent->DOB = Carbon::parse($request->dob);
        $newStudent->POB = $request->pob;
        $newStudent->is_active = $request->has('is_active');
        if($newStudent->save()){
            return redirect()->route('backend-student-show', ['id' => $newStudent->id]);
        }else{
            return Redirect::back()->withInput()->withErrors(['msg', 'The Message']);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student,$id)
    {
        $data = $student->where("id",$id)->first();
        if($data){
            return view('backend.student.view')->with("data",$data);
        }else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
