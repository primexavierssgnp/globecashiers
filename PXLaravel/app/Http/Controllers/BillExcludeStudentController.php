<?php

namespace App\Http\Controllers;

use App\Model\BillExcludeStudent;
use Illuminate\Http\Request;

class BillExcludeStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\BillExcludeStudent  $billExcludeStudent
     * @return \Illuminate\Http\Response
     */
    public function show(BillExcludeStudent $billExcludeStudent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\BillExcludeStudent  $billExcludeStudent
     * @return \Illuminate\Http\Response
     */
    public function edit(BillExcludeStudent $billExcludeStudent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\BillExcludeStudent  $billExcludeStudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BillExcludeStudent $billExcludeStudent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\BillExcludeStudent  $billExcludeStudent
     * @return \Illuminate\Http\Response
     */
    public function destroy(BillExcludeStudent $billExcludeStudent)
    {
        //
    }
}
