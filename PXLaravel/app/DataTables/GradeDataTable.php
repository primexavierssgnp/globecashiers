<?php

namespace App\DataTables;

use App\Model\Grade;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class GradeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function(Grade $grade) {
                return '<a href="'. route("backend-grade-show",$grade->id) .'" class="btn cur-p btn-primary"><i class="fa fa-id-badge" aria-hidden="true"></i></a>
                        <a href="'. route("backend-grade-show",$grade->id) .'" class="btn cur-p btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="'. route("backend-grade-show",$grade->id) .'" class="btn cur-p btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Grade $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Grade $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('grade-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('name'),
            Column::make('is_active'),
            Column::make('created_at'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(150)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Grade_' . date('YmdHis');
    }
}
