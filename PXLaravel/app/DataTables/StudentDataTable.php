<?php

namespace App\DataTables;

use App\Model\Student;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class StudentDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function(Student $student) {
                return '<a href="'. route("backend-student-show",$student->id) .'" class="btn cur-p btn-primary"><i class="fa fa-id-badge" aria-hidden="true"></i></a>
                        <a href="'. route("backend-student-update",$student->id) .'" class="btn cur-p btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="'. route("backend-student-delete",$student->id) .'" class="btn cur-p btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        <a href="'. route("backend-student",$student->id) .'" class="btn cur-p btn-success"><i class="fa fa-credit-card" aria-hidden="true"></i>                        </a>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Student $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Student $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('student-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('NIS'),
            Column::make('NISN'),
            Column::make('name'),
            Column::make('created_at'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(200)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Student_' . date('YmdHis');
    }
}
