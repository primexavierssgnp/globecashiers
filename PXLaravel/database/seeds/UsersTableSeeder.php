<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => "SuperAdmin",
            'email' => "SuperAdmin@primexaviers.com",
            'password' => bcrypt('superadmin'),
            'level_id' => 1,
            'is_active' => true,
            'email_verified_at' => Carbon\Carbon::now(),
            'created_at' => Carbon\Carbon::now()
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => "Admin",
            'email' => "Admin@primexaviers.com",
            'password' => bcrypt('admin'),
            'level_id' => 2,
            'is_active' => true,
            'email_verified_at' => Carbon\Carbon::now(),
            'created_at' => Carbon\Carbon::now()
        ]);
    }
}
