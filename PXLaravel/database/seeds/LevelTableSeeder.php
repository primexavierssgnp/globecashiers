<?php

use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
            'id' => 1,
            'name' => 'Super Admin',
            'description' => 'Super Admin',
            'access' => '{"page": "all", "update": "all", "delete": "all", "add": "all"}',
            'created_at' => Carbon\Carbon::now()
        ]);
        DB::table('levels')->insert([
            'id' => 2,
            'name' => 'Admin',
            'description' => 'Admin',
            'access' => '{"page": ["user","product","grade","student","degree","bill","payment"], "update":  ["product","grade","student","degree","bill","payment"], "delete": ["product","grade","student","degree","bill","payment"], "add":  ["product","grade","student","degree","bill","payment"]}',
            'created_at' => Carbon\Carbon::now()
        ]);
        DB::table('levels')->insert([
            'id' => 3,
            'name' => 'Chasier',
            'description' => 'Chasier',
            'access' => '{"page": ["product","student","bill","payment"], "update": ["product","bill"], "delete": ["product","bill"], "add": ["product","degree","bill","payment"]}',
            'created_at' => Carbon\Carbon::now()
        ]);
        DB::table('levels')->insert([
            'id' => 4,
            'name' => 'Teacher',
            'description' => 'Teacher',
            'access' => '{"page": ["student","grade"], "update": ["student","grade"], "delete": ["student","grade"], "add": ["student","grade"]}',
            'created_at' => Carbon\Carbon::now()
        ]);
        DB::table('levels')->insert([
            'id' => 5,
            'name' => 'Tata Usaha',
            'description' => 'Tata Usaha',
            'access' => '{"page": ["student","grade"], "update": ["student","grade"], "delete": ["student","grade"], "add": ["student","grade"]}',
            'created_at' => Carbon\Carbon::now()
        ]);
        DB::table('levels')->insert([
            'id' => 6,
            'name' => 'Accounting',
            'description' => 'Accounting',
            'access' => '{"page": ["product","bill","payment"], "update": ["product","bill","payment"], "delete":  ["product","bill","payment"], "add": ["product","degree","bill","payment"]}',
            'created_at' => Carbon\Carbon::now()
        ]);
    }
}
