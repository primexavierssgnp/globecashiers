<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'NIS' => 9999999999,
            'NISN' => 9999999999,
            'name' => "ALDI AFANDI",
            'DOB' => Carbon\Carbon::now(),
            'POB' => "Jakarta",
            'is_active' => True
        ]);
    }
}
