<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::prefix('backend')->group(function () {
        Route::get('/', 'HomeController@dashboard')->name('backend-dashboard');
        Route::prefix('bill')->group(function () {
            Route::get('/', 'BillController@index')->name('backend-bill');
            Route::get('/create', 'BillController@create')->name('backend-bill-create');
            Route::get('/show/{id}', 'BillController@show')->name('backend-bill-show');
            Route::get('/update/{id}', 'BillController@update')->name('backend-bill-update');
            Route::get('/delete/{id}', 'BillController@destroy')->name('backend-bill-delete');
            Route::post('/store', 'BillController@store')->name('backend-bill-store');
        });
        Route::prefix('billType')->group(function () {
            Route::get('/', 'BillTypeController@index')->name('backend-billType');
            Route::get('/create', 'BillTypeController@create')->name('backend-billType-create');
            Route::get('/show/{id}', 'BillTypeController@show')->name('backend-billType-show');
            Route::get('/update/{id}', 'BillTypeController@update')->name('backend-billType-update');
            Route::get('/delete/{id}', 'BillTypeController@destroy')->name('backend-billType-delete');
            Route::post('/store', 'BillContBillTypeControllerroller@store')->name('backend-billType-store');
        });   
        Route::prefix('degree')->group(function () {
            Route::get('/', 'DegreeController@index')->name('backend-degree');
            Route::get('/create', 'DegreeController@create')->name('backend-degree-create');
            Route::get('/show/{id}', 'DegreeController@show')->name('backend-degree-show');
            Route::get('/update/{id}', 'DegreeController@update')->name('backend-degree-update');
            Route::get('/delete/{id}', 'DegreeController@destroy')->name('backend-degree-update');
            Route::post('/store', 'DegreeController@store')->name('backend-degree-store');
        });
        Route::prefix('grade')->group(function () {
            Route::get('/', 'GradeController@index')->name('backend-grade');
            Route::get('/create', 'GradeController@create')->name('backend-grade-create');
            Route::get('/show/{id}', 'GradeController@show')->name('backend-grade-show');
            Route::get('/update/{id}', 'GradeController@update')->name('backend-grade-update');
            Route::get('/delete/{id}', 'GradeController@destroy')->name('backend-grade-delete');
            Route::post('/store', 'GradeController@store')->name('backend-grade-store');
        });
        Route::prefix('payment')->group(function () {
            Route::get('/', 'PaymentController@index')->name('backend-payment');
            Route::get('/create', 'PaymentController@create')->name('backend-payment-create');
            Route::get('/show/{id}', 'PaymentController@show')->name('backend-payment-show');
            Route::get('/update/{id}', 'PaymentController@update')->name('backend-payment-update');
            Route::get('/delete/{id}', 'PaymentController@destroy')->name('backend-payments-delete');
            Route::post('/store', 'PaymentController@store')->name('backend-payments-store');
        });
        Route::prefix('product')->group(function () {
            Route::get('/', 'ProductController@index')->name('backend-product');
            Route::get('/create', 'ProductController@create')->name('backend-product-create');
            Route::get('/show/{id}', 'ProductController@show')->name('backend-product-show');
            Route::get('/update/{id}', 'ProductController@update')->name('backend-product-update');
            Route::get('/delete/{id}', 'ProductController@destroy')->name('backend-product-delete');
            Route::post('/store', 'ProductController@store')->name('backend-product-store');
        });
        Route::prefix('student')->group(function () {
            Route::get('/', 'StudentController@index')->name('backend-student');
            Route::get('/create', 'StudentController@create')->name('backend-student-create');
            Route::get('/show/{id}', 'StudentController@show')->name('backend-student-show');
            Route::get('/update/{id}', 'StudentController@update')->name('backend-student-update');
            Route::get('/delete/{id}', 'StudentController@destroy')->name('backend-student-delete');
            Route::post('/store', 'StudentController@store')->name('backend-student-store');
        });
        Route::group(['middleware' => ['level']], function () {
            Route::prefix('user')->group(function () {               
                Route::get('/', 'UsersController@index')->name('backend-user');
                Route::get('/create', 'UsersController@create')->name('backend-user-create');
                Route::get('/show/{id}', 'UsersController@show')->name('backend-user-show');
                Route::get('/update/{id}', 'UsersController@update')->name('backend-user-update');
                Route::get('/delete/{id}', 'UsersController@destroy')->name('backend-user-delete');
                Route::post('/store', 'UsersController@store')->name('backend-user-store');
            });            
            Route::prefix('level')->group(function () {
                Route::get('/', 'LevelController@index')->name('backend-level');
                Route::get('/create', 'LevelController@create')->name('backend-level-create');
                Route::get('/show/{id}', 'LevelController@show')->name('backend-level-show');
                Route::get('/update/{id}', 'LevelController@update')->name('backend-level-update');
                Route::get('/delete/{id}', 'LevelController@destroy')->name('backend-level-delete');
                Route::post('/store', 'LevelController@store')->name('backend-level-store');
            });
        });
    });
});
