@extends('layouts.backend')

@section('title', 'Bills')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h4 class="c-grey-900 mB-20">Show Bills</h4>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
@endsection