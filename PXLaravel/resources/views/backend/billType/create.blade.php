@extends('layouts.backend')

@section('title', 'Type Bill')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h6 class="c-grey-900">Add Type Bill</h6>
                    <div class="mT-30">
                        <form method="POST" action="{{ route('backend-bill-store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="inputName">Name</label>
                                <input name="name" type="text" class="form-control" id="inputName" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="inputDegree">Type</label>
                                <select required="required" id="inputDegree" class="form-control" name="degree_id">
                                    <option value="" selected="selected">Pick One</option>
                                    <option value="byDate">Period</option>
                                    <option value="byCount">Amount</option>
                                    <option value="custom">Custom</option>
                                    <option value="spp">SPP (1 Bulan)</option>
                                    <option value="spp">SPP (FULL)</option>
                                </select>
                            </div>
                            <div class="form-group" id="howManyForm">
                                <label for="inputName">How Many?</label>
                                <input name="many" type="number" class="form-control" id="howMany" placeholder="PerTimes">
                            </div>
                            <div class="form-group" id="deadlineTimeForm">
                                <label for="inputName">Deadline Date</label>
                                <input name="many" type="number" class="form-control" id="deadlineTime" placeholder="Deadline Date" max="31" min="1">
                            </div>
                            <div class="form-group" id="startDateForm">
                                <label for="inputName">Start Date</label>
                                <input name="start" type="date" class="form-control" id="startDate">
                            </div>
                            <div class="form-group" id="endDateForm">
                                <label for="inputName">End Date</label>
                                <input name="end" type="date" class="form-control" id="endDate">
                            </div>
                            <div class="form-group">
                                <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                                    <input name="is_active" type="checkbox" id="is_active" name="is_active" class="peer">
                                    <label for="is_active" class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Active</span></label>
                                    <input name="is_active" type="checkbox" id="yearly" name="yearly" class="peer">
                                    <label for="yearly" class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Every Year?</span></label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
<script>    
    $("#howManyForm").hide();
    $("#endDateForm").hide();
    $("#startDateForm").hide();
    $("#deadlineTimeForm").hide();
</script>
@endsection