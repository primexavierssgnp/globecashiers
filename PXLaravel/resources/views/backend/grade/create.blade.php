@extends('layouts.backend')

@section('title', 'Grades')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h6 class="c-grey-900">Add Grades</h6>
                    <div class="mT-30">
                        <form method="POST" action="{{ route('backend-grade-store') }}">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputName">Name</label>
                                    <input name="name" type="text" class="form-control" id="inputName" placeholder="name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputLevel">Level</label>
                                    <input name="level" type="number" class="form-control" id="inputLevel" placeholder="Level">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputDegree">Degree</label>
                                <select id="inputDegree" class="form-control" name="degree_id">
                                    <option value="" selected="selected">Pick One</option>
                                    @foreach($degrees as $degree)
                                        <option value="{{$degree->id}}">{{$degree->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Description</label>
                                <textarea name="desc" class="form-control" id="inputDescription" placeholder="Grade Description" ></textarea>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                                    <input name="is_active" type="checkbox" id="inputCall2" name="is_active" class="peer">
                                    <label for="inputCall2" class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Active</span></label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
@endsection