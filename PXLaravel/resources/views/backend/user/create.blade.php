@extends('layouts.backend')

@section('title', 'Users')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">            
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h6 class="c-grey-900">Add User</h6>
                    <div class="mT-30">
                        <form method="POST" action="{{ route('backend-user-store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="inputName">Name</label>
                                <input name="name" type="text" class="form-control" id="inputName" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="inputName">access</label>
                                <input name="access" type="text" class="form-control" id="inputName" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="inputLevel">Description</label>
                                <textarea name="desc" class="form-control" id="inputLevel" placeholder="Stock" >
                                </textarea>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                                    <input name="is_active" type="checkbox" id="inputCall2" name="is_active" class="peer">
                                    <label for="inputCall2" class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Active</span></label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
@endsection