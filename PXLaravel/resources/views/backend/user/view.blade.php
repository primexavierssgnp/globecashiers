@extends('layouts.backend')

@section('title', 'Users')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h4 class="c-grey-900 mB-20">Show Users</h4>
                    {{$data->id}}<br>
                    {{$data->name}}<br>
                    {{$data->access}}<br>
                    {{$data->description}}<br>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
@endsection