@extends('layouts.backend')

@section('title', 'Students')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h4 class="c-grey-900 mB-20">Show Students</h4>
                    {{$data->id}}<br>
                    {{$data->name}}<br>
                    {{$data->NISN}}<br>
                    {{$data->NIS}}<br>
                    {{$data->DOB}}<br>
                    {{$data->POB}}<br>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
@endsection