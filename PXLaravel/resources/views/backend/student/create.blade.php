@extends('layouts.backend')

@section('title', 'Students')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h6 class="c-grey-900">Add Students</h6>
                    <div class="mT-30">
                        <form method="POST" action="{{ route('backend-student-store') }}">
                            @csrf                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputName">NISN</label>
                                    <input name="nisn" type="number" class="form-control" id="inputName" placeholder="NISN">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputLevel">NIS</label>
                                    <input name="nis" type="number" class="form-control" id="inputLevel" placeholder="NIS">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName">Name</label>
                                <input name="name" type="text" class="form-control" id="inputName" placeholder="Name">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputpob">Place Of Birth</label>
                                    <input name="pob" type="text" class="form-control" id="inputName" placeholder="Place of Birth">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputdob">Date Of Birth</label>
                                    <input name="dob" type="text" class="form-control bdc-grey-200 start-date" placeholder="Date Of Birth" data-provide="datepicker">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                                    <input name="is_active" type="checkbox" id="inputCall2" name="is_active" class="peer">
                                    <label for="inputCall2" class="peers peer-greed js-sb ai-c"><span class="peer peer-greed">Active</span></label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
@endsection