@extends('layouts.backend')

@section('title', 'Products')

@section('customStyle')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="bgc-white bd bdrs-3 p-20 mB-20">
                    <h6 class="c-grey-900">Add Product</h6>
                    <div class="mT-30">
                        <form method="POST" action="{{ route('backend-product-store') }}">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputName">Name</label>
                                    <input name="name" type="text" class="form-control" id="inputName" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputLevel">Price</label>
                                    <input name="price" type="number" class="form-control" id="inputLevel" placeholder="Price">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputLevel">Start Stock</label>
                                    <input name="stock" type="number" class="form-control" id="inputLevel" placeholder="Stock">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customScript')
@endsection